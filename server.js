var express = require('express');
var app = express();
var io = require('socket.io');
var http = require('http');
var videoroom =require("./models/videoroom");
var path = require('path');
// socket.io 
var server = require('http').Server(app);
var io = require('socket.io')(server);



server.listen(process.env.PORT || 8080);

// json parsing code.
var parser = require("body-parser");
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

app.use(express.static(path.join(__dirname, 'public')));

// enabling access control origin
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
    res.sendFile('index.html', {root: __dirname+'/views/'});
});

app.get('/usermedia.js', function (req, res) {
    res.sendFile('usermedia.js', {root: __dirname+'/views/'});
});

app.get('/peerimpl.js', function (req, res) {
    res.sendFile('peerimpl.js', {root: __dirname+'/views/'});
});

app.get('/socketimpl.js', function (req, res) {
    res.sendFile('socketimpl.js', {root: __dirname+'/views/'});
});

io.on('connection', function (socket) {
    console.log("user is connected");
    socket.on('userconnect', function (data) {

        var videodata = new videoroom(JSON.parse(data));
        console.log(videodata.videoid);
        socket.join(videodata.roomname);
        // send to all clients except the one who joined the conversation.
        socket.broadcast.to(videodata.roomname).emit('newjoinee', videodata.videoid);
        // io.in(videodata.roomname).emit('newjoinee', videodata.id);
    });
    socket.on('disconnect', function () {
        console.log("user has been disconnected");
    });
});


