var peer = new Peer({
    key: 'yu809arn510v0a4i',
   // host: '192.168.1.8', 
   // port: 8668,
   // path: '/peerjs',
    config: { 'iceServers': [
    { url: 'stun:stun01.sipphone.com' },
    { url: 'stun:stun.ekiga.net' },
    { url: 'stun:stun.fwdnet.net' },
    { url: 'stun:stun.ideasip.com' },
    { url: 'stun:stun.iptel.org' },
    { url: 'stun:stun.rixtelecom.se' },
    { url: 'stun:stun.schlund.de' },
    { url: 'stun:stun.l.google.com:19302' },
    { url: 'stun:stun1.l.google.com:19302' },
    { url: 'stun:stun2.l.google.com:19302' },
    { url: 'stun:stun3.l.google.com:19302' },
    { url: 'stun:stun4.l.google.com:19302' },
    { url: 'stun:stunserver.org' },
    { url: 'stun:stun.softjoys.com' },
    { url: 'stun:stun.voiparound.com' },
    { url: 'stun:stun.voipbuster.com' },
    { url: 'stun:stun.voipstunt.com' },
    { url: 'stun:stun.voxgratia.org' },
    { url: 'stun:stun.xten.com' }
  ]
    }
});
var peerid;
var call;
var counter = 0;
peer.on('open', function (id) {
    console.log("Let me know the id " + id);
    peerid = id;
    sendRtcInfo(peerid);
});

peer.on('call', function (call) {
  //  document.getElementById('answercall').innerHTML = "answering peer call";
    call.answer(mediastream);
    call.on('stream', function (stream) {
    //    document.getElementById('answercall').innerHTML = "<br> streaming video from the opposite side";
        setVideo(stream);
    });
});

function sendRtcInfo(id){
    var roomname = "defaultroom";
    var packet = {
        roomname:roomname,
        videoid:id
    }
    socket.emit('userconnect', JSON.stringify(packet));
    console.log('id ' + id + " and the roomname " + roomname);
}

function joinPeer(remotePeerId) {
    //console.log("The remote peer id "remotePeerId);
  //  var desinationPeer = document.getElementById('destinationid').value;
    call= peer.call(remotePeerId, mediastream);
    call.on('stream', function (stream) {
     //   document.getElementById('answercall').innerHTML = "<br> streaming video from the opposite side";
        counter++;
        //var id = "remotevideo" + counter;
        setVideo(stream);
    });
    counter++;
}