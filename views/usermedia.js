navigator.getUserMedia || (navigator.getUserMedia = navigator.mozGetUserMedia
                                 || navigator.webkitGetUserMedia || navigator.msGetUserMedia);
if (navigator.getUserMedia) {
    navigator.getUserMedia({ video: true, audio: true, toString: function () {
        return "video,audio";
    }
    }, onSuccess, onError);
} else {
    alert("getUserMedia not supported");
}

function onSuccess(stream) {

    //var source;
    //if (window.webkitURL) {
    //    source = window.webkitURL.createObjectURL(stream);
    //} else {
    //    source = stream; // Opera and Firefox
    //}

    //var video = document.getElementById("localvideo");
    //video.autoplay = true;
    //video.src = source;
    setVideo(stream);
    mediastream = stream;
}
function onError(stream) {
    alert("error in " + stream);
}

function setVideo(stream) {
    var source;
    //var videoStream = document.getElementById(id);
    var videoStream;
    if (videoStream == null) {
        videoStream = document.createElement("video");
        //  videoStream.id = id;
        var division = document.createElement("div");
        division.classList.add("video-container");
        division.appendChild(videoStream);
        document.getElementById('videodivision').appendChild(division);
    }

    if (window.webkitURL) {
        source = window.webkitURL.createObjectURL(stream);
    } else {
        source = stream; // Opera and Firefox
    }
    //  videoStream.style.height = 300;
    videoStream.src = source;
    videoStream.setAttribute("controls", "controls");
    videoStream.autoplay = true;
    var audioTracks = stream.getAudioTracks();
  for (var i = 0, l = audioTracks.length; i < l; i++) {
      console.log("Writing audio tracks " + audioTracks[i])
   // audioTracks[i].enabled = !audioTracks[i].enabled;
  }
    setClassName();
}

function setClassName() {
    var elements = document.getElementById('videodivision').children;
    var elementsNo = $('.video-container').length;
    console.log('No of elements ' + elementsNo + " length " + elements.length);
    if(elementsNo == 1){
        addClassNameToAllElements('','one', elements);
    }
    if(elementsNo == 2){
        addClassNameToAllElements('one', 'two', elements);
    }
     if(elementsNo == 3){
        addClassNameToAllElements('two', 'three', elements);
    }
     if(elementsNo >=4 ){
        addClassNameToAllElements('three', 'four', elements);
    }
}

function addClassNameToAllElements(oldClass, newClass, elements){
    $(document).ready(function () {
        $('.video-container').removeClass(oldClass);
        $('.video-container').addClass(newClass);
    });
    //for(var i=0 ; i< elements.length; i++){
    //    elements[i].className = className;
    //}
}